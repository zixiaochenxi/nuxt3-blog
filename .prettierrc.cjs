module.exports = {
  printWidth: 80, // 最大行宽
  tabWidth: 2, // 使用2个空格作为缩进
  useTabs: false, // 不使用制表符（tab），改用空格
  semi: true, // 结尾处需要分号
  singleQuote: false, // 使用单引号
  trailingComma: 'es5', // 在对象或数组最后一个元素后面添加逗号
  endOfLine: 'lf', // 使用LF（\n）作为行结束符
};

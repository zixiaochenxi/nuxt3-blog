// https://nuxt.com/docs/api/configuration/nuxt-config

export default defineNuxtConfig({
  // 开发模式下开启 线上关闭
  devtools: { enabled: process.env.NODE_ENV === 'development' ? true : true },
  devServer: {
    port: 3333,
    host: '0.0.0.0',
  },
  css: [
    // 引入全局 Scss 文件
    './assets/styles/main.scss',
  ],
  modules: ['@element-plus/nuxt', '@pinia/nuxt', '@nuxtjs/tailwindcss'],
});

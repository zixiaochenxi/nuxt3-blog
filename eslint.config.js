import globals from "globals";
import tseslint from "typescript-eslint";
import pluginVue from "eslint-plugin-vue";
// import typeScriptParser from "@typescript-eslint/parser";

export default [
  {
    languageOptions: {
      parser: tseslint.parser,
      parserOptions: {
        parser: tseslint.parser,
        ecmaVersion: "latest",
        sourceType: "module",
        project: ["./tsconfig.json"],
        extraFileExtensions: [".vue"],
      },
      globals: {
        ...globals.browser, // browser 环境
        ...globals.node, // node
        ...globals.es2021, // es2021
      },
    },
  },
  ...tseslint.configs.recommendedTypeChecked,
  ...pluginVue.configs["flat/essential"],
  {
    ignores: [".nuxt/", ".output/", "eslint.config.js", ".prettierrc.cjs"], // 忽略文件
  },
  {
    rules: {
      // 自定义规则
      "vue/multi-word-component-names": "off", // 组件名称
      "@typescript-eslint/no-explicit-any": "off", // 允许 any
      // 关闭 promise/always-return
      "promise/always-return": "off",
      // 关闭 Promise 导致多种问题的校验
      "@typescript-eslint/no-floating-promises": "off",
    },
  },
];

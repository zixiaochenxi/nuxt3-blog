// store/index.ts
import { defineStore } from "pinia";

export const useMyStore = defineStore({
  // 定义你的状态和方法
  id: "myStore",
  state: () => ({
    count: 0,
  }),
  actions: {
    increment() {
      this.count++;
    },
  },
});

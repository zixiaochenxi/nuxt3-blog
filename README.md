# nuxt-vite-blog

#### 介绍

多人合作项目 一起写 nuxt 博客

#### 软件所用技术

Nuxt3

#### 安装教程

1.  拉取代码 git clone https://gitee.com/mrzym/nuxt-vite-blog.git
2.  下载依赖 pnpm i
3.  运行项目 pnpm run dev

#### 参与贡献

方式一
1. 加入项目 成为开发者 在dev分支上开发
2. 开发者只可以在dev上提交代码 master分支由仓库管理者合并代码

方式二 fork

1.  Fork 本仓库
2.  在自己仓库的dev分支开发
3.  在提交代码前将代码更新到最新
4.  提交代码
5.  新建 Pull Request 到主仓库 也就是 https://gitee.com/mrzym/nuxt-vite-blog 推送到dev分支
6.  通知我审核

#### 提交代码说明

提交代码 格式： 提交类型 + ':' + 描述 描述主要做了啥
例如：fix:  修复了 xxx bug
     feat:  新增xxx功能/页面
     docs:  新增xxx功能 文档 可以是描述文档 也可以是使用方法

```js
'build', // 编译相关的修改，例如发布版本、对项目构建或者依赖的改动
'feat', // 新功能
'fix', // 修补bug
'docs', // 文档修改
'style', // 代码格式修改, 注意不是 css 修改
'refactor', // 重构
'perf', // 优化相关，比如提升性能、体验
'test', // 测试用例修改
'revert', // 代码回滚
'ci', // 持续集成修改
'config', // 配置修改
'chore', // 其他改动
```

由于代码做了提交校验 所以需要按照规范来修改代码 才可以提交成功 不熟悉规则的可以学习一下 eslint、commitlint

### 代码提交合并步骤

在开发过程中 希望大家尽量先拉取代码 处理好本地冲突再进行提交合并

方式一 dev分支开发
```
1、首先加入QQ群 告诉群主想成为仓库开发者 群主邀请你 

2、在gitee上拉取仓库代码 在dev分支开发

3、开发完成后 先缓存代码到本地
git stash

3、然后进行最新代码拉取
git pull

4、然后取出缓存 解决本地冲突
git stash pop 

5、然后git push 推送代码到远程仓库 dev分支

6、最后 仓库管理者会检查并且合并代码到master分支
```

方式二 fork
如何提交代码
```
1、首先在gitee上同步自己fork仓库代码到最新版本 尽量在dev分支开发

2、开发完成后 先缓存代码到本地
git stash

3、然后进行最新代码拉取
git pull

4、然后取出缓存 解决本地冲突
git stash pop 

5、然后git push 推送代码到远程 fork 仓库

6、最后 在仓库里点击 Pull Request 创建合并 推送给主仓库的dev分支
```

### 团队成员记录

可以在docs里添加自己qq昵称或者是gitee账户昵称的md文档 编写自己做了些啥 方便仓库成员查看

### 项目介绍
|-- nuxt3-blog
    |-- .gitignore // git忽略文件
    |-- .prettierignore // prettier忽略文件
    |-- .prettierrc.cjs // prettier配置文件
    |-- app.vue // 根组件
    |-- commitlint.config.cjs // commitlint配置文件
    |-- eslint.config.js // eslint配置文件
    |-- LICENSE // 开源协议
    |-- nuxt.config.ts // nuxt配置文件
    |-- package-lock.json
    |-- package.json // 项目依赖
    |-- pnpm-lock.yaml
    |-- README.en.md
    |-- README.md
    |-- tsconfig.json
    |-- .husky
    |   |-- commit-msg
    |   |-- pre-commit
    |-- .output // nuxt编译后的文件
    |-- components // 项目组件
    |-- docs // 项目文档
    |-- pages // 项目页面文件夹
    |-- utils // 项目工具
    |-- hooks // 项目hook
    |-- public // 项目静态资源文件夹
    |   |-- favicon.ico
    |-- server 
        |-- tsconfig.json
